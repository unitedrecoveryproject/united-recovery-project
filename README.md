Located in South Florida, United Recovery Project is a luxury drug and alcohol rehabilitation center. With a low client-to-staff ratio, we deliver customized and comprehensive care to those struggling with a substance use disorder.

Website: https://www.unitedrecoveryproject.com/
